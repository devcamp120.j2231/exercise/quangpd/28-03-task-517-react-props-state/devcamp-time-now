import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Time from './Components/Times';

function App() {
  return (
    <div>
      <Time/>
    </div>
  );
}

export default App;
